package com.victorlapin.flasher.view

import moxy.MvpView
import moxy.viewstate.strategy.SkipStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(SkipStrategy::class)
interface MainActivityView : MvpView {
    fun askFingerprint()
    fun cancelFingerprint()
}