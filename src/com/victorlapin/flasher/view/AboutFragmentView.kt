package com.victorlapin.flasher.view

import com.victorlapin.flasher.model.repository.AboutRepository
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndStrategy
import moxy.viewstate.strategy.StateStrategyType

interface AboutFragmentView : MvpView {
    @StateStrategyType(AddToEndStrategy::class)
    fun setData(data: List<AboutRepository.ListItem>)
}