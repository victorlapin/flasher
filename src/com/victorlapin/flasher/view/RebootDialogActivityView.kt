package com.victorlapin.flasher.view

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndStrategy
import moxy.viewstate.strategy.StateStrategyType

interface RebootDialogActivityView : MvpView {
    @StateStrategyType(AddToEndStrategy::class)
    fun showRebootDialog()

    @StateStrategyType(AddToEndStrategy::class)
    fun askFingerprint()
}