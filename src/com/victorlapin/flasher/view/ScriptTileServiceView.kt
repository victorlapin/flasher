package com.victorlapin.flasher.view

import com.victorlapin.flasher.model.EventArgs
import moxy.MvpView
import moxy.viewstate.strategy.SkipStrategy
import moxy.viewstate.strategy.StateStrategyType

interface ScriptTileServiceView : MvpView {
    @StateStrategyType(SkipStrategy::class)
    fun showInfoToast(args: EventArgs)

    @StateStrategyType(SkipStrategy::class)
    fun showInfoToast(message: String)

    @StateStrategyType(SkipStrategy::class)
    fun showRebootDialog()
}